let express = require('express');
let server = express();

let body_parser = require('body-parser');   
server.use(body_parser.urlencoded({extended:true}));

server.use('/personen', express.static(__dirname + '/public'));

server.set('view engine', 'ejs');

let database = [
    {'id' : 'hei', 'name' : 'Heinrich', 'vorname' : 'Leon', 'adresse': 'Straße 1', 'telefon': 201019},
    {'id' : 'mei', 'name' : 'Meier', 'vorname' : 'Tom', 'adresse': 'Straße 2', 'telefon': 0657387},
    {'id' : 'fis', 'name' : 'Fischer', 'vorname' : 'Hans', 'adresse': 'Straße 3', 'telefon': 289347}  
];

server.get('/personen/search', (req, res) => {
    
    let id = req.query.id;  
    let result;
    
    for(let person of database) {
        
        if(id === person.id) {
            result = person;
            break;
        }
        
    }
    
    if(result === undefined) {
        result = "Not found...";
    }
    
    res.send(result); 
});

server.get('/personen/all', (req, res) => {

    res.render('alle', {data: database});

})

server.post('/personen/save', (req, res) => {
    let answer;
    let person = req.body;  
    
    let found = false;
    for(let i = 0; i < database.length; i++) {
        let p = database[i];
        if(p.id === person.id) {
            database[i] = person;  
            answer = 'Person wurde geändert.';
            found = true;
            break;
        }
    }
    
    if(!found) {   
        database.push(person);
        answer = 'Person wurde neu angelegt.';
    }
    
    res.send(answer);
});

server.post('/personen/delete', (req, res) => {
    let id = req.body.id;
    let index = -1;
    let answer = '';
    
    for(let i = 0; i < database.length; i++) {
        let p = database[i];
        if(p.id === id) {
            index = i;
            break;
        }
    }
    
    if (index === -1) {
        answer = "Person nicht gefunden";
    } else {
        answer = "Person erfolgreich gelöscht";
        database.splice(index, 1);  
    }
    
    res.send(answer);
    
});




server.listen(2019, 'localhost', () => {
    console.log("Server läuft auf Port 2019 ...");  //hier Konsolenausgabe als Callback-Funktion -> nur ausgeführt, falls Server wirklich läuft
});

