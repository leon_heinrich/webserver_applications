let express = require('express');
let server = express();

let body_parser = require('body-parser');   
server.use(body_parser.urlencoded({extended:true}));

server.use('/restaurants', express.static(__dirname + '/public'));


let MongoClient = require('mongodb').MongoClient;
let db = null;

MongoClient.connect("mongodb://localhost:27017", (err, client) => {
    db = client.db('test');
    console.log(db);
});

server.get('/restaurants/search', (req, res) => {
    let borough = req.query.borough;
    let cuisine = req.query.cuisine;
    if(borough === "" && cuisine != "") {
        db.collection('restaurants').find({'cuisine':cuisine}).toArray(function(err, result) {
            if(Object.keys(result).length === 0) {
                res.send("Nichts gefunden");
            } else {
                res.render('mongodbtemplate', {cuisine:cuisine, arr: result});
            }
        });
    } else if(borough != "" && cuisine === "") {
        db.collection('restaurants').find({'borough':borough}).toArray(function(err, result) {
            if(Object.keys(result).length === 0) {
                res.send("Nichts gefunden");
            } else {
                res.render('mongodbtemplate', {borough:borough, arr: result});
            }
        });
    } else if(borough === "" && cuisine === "") {
        res.send("Du hast nichts eingegeben!")
    } else {
        db.collection('restaurants').find({'borough':borough, 'cuisine':cuisine}).toArray(function(err, result) {
            if(Object.keys(result).length === 0) {
                res.send("Nichts gefunden");
            } else {
                res.render('mongodbtemplate', {borough:borough, cuisine:cuisine, arr: result});
            }
        });
    }
});

server.listen(2019, 'localhost', () => {
    console.log("Server läuft auf Port 2019 ...");  
});