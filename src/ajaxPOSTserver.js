let express = require('express');
let server = express();

let body_parser = require('body-parser');    
server.use(body_parser.urlencoded({extended:true}));

server.use('/MyWebApp', express.static(__dirname + '/public'));

let database = [
    {'email' : 'a@b.c', 'name' : 'Heinrich', 'vorname' : 'Leon'},
    {'email' : 'd@e.f', 'name' : 'Meier', 'vorname' : 'Tom'},
    {'email' : 'g@h.i', 'name' : 'Fischer', 'vorname' : 'Hans'}  
];

server.post('/MyWebApp/dbGet', function(req, res) {
   
    let db_mail = req.body.email;  
    for(let i = 0; i < database.length; i++) {
        if(db_mail == database[i].email) {
            let foundEntry = database[i];
            res.send({'response': '{' + foundEntry.email + ', ' + foundEntry.name + ', ' + foundEntry.vorname + '}'});
        }
    }
    res.send({'response': "Keine Daten gefunden!"});    
});
    
server.post('/MyWebApp/dbSave', function(req, res) {
    let db_email = req.body.email;
    let db_name = req.body.name;
    let db_vorname = req.body.vorname;
    database.push({'email': db_email, 'name': db_name, 'vorname': db_vorname});
    
    let checkLastEntry = database[database.length-1];
    console.log(database);  //aktualisierte DB
    res.send({'response': '{' + checkLastEntry.email + ', ' + checkLastEntry.name + ', ' + checkLastEntry.vorname + '}'});
});
    

server.post('/MyWebApp/dbAsTable', function(req, resp) {
        server.set('view engine', 'ejs');
        resp.render('dataTemplate', {data: database});
});


server.listen(2019);

console.log("Server läuft auf Port 2019 ...");